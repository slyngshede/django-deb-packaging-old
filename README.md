# django-deb-packaging

Bootstrap Debian package building for your Django project.
The script will create the required files for building a Debian
package for a Django project and optionally start a new project.

```
$ ./bootstrap -l <license> -t <directory where your project will live> <project name>
$ cd <directory where your project will live>
$ dch # Start changelog for Debian package.
```